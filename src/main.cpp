#include <iostream>



#include <QtGui>


using namespace std;

int main(int argc, char** argv)
{
    cout << "namib main routine" << endl;
    
// first main attempt for setting up the environment,
// just copied from http://qt-project.org/doc/qt-4.8/gettingstartedqt.html#using-a-qmainwindow

    QApplication app(argc, argv);
 
    QTextEdit *textEdit = new QTextEdit;
    QPushButton *quitButton = new QPushButton("Quit");
 
    QObject::connect(quitButton, SIGNAL(clicked()), qApp, SLOT(quit()));
 
    QVBoxLayout *layout = new QVBoxLayout;
    layout->addWidget(textEdit);
    layout->addWidget(quitButton);
 
    QWidget window;
    window.setLayout(layout);
 
    window.show();
 
    return app.exec();
}
